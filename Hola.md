# Extracción de datos (Web scraping)

índice:

1er bloque.

1. Internet y navegadores
    1.1 Peticiones HTTP
    1.2 Navegadores y contenido HTML
2. Extracción de contenido estático
    2.1 Define tus objetivos
    2.2 Interpretación de contenido HTML
    2.3 Extracción de datos usando JavaScript
    2.4 Ejemplos
    2.5 Ejercicios

